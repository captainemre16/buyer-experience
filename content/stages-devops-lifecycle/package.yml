---
  title: Package
  description: Learn how GitLab creates a consistent and dependable software supply chain with built-in package management. view more here!
  components:
    - name: sdl-cta
      data:
        title: Package
        aos_animation: fade-down
        aos_duration: 500
        subtitle: |
          Create a consistent and dependable software supply chain with built-in package management.
        text: |
          GitLab enables teams to package their applications and dependencies, manage containers, and build artifacts with ease. The private, secure, container and package registry are built-in and preconfigured out-of-the box to work seamlessly with GitLab source code management and CI/CD pipelines. Ensure DevOps acceleration and a faster time to market with automated software pipelines that flow freely without interruption.
        image:
          url: /nuxt-images/icons/devops/package-colour.svg
          alt: Package
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Package Registry
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Every team needs a place to store their packages and dependencies. GitLab aims to provide a comprehensive solution, integrated into our single application, that supports package management for all commonly used languages and binary formats.
            link:
              href: https://docs.gitlab.com/ee/user/packages/package_registry/
              text: Learn More
              data_ga_name: package registry learn more
              data_ga_location: body
          - title: Container Registry
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              A secure and private registry for Docker images built-in to GitLab. Creating, pushing, and retrieving images works out of the box with GitLab CI/CD.
            link:
              href: https://docs.gitlab.com/ee/user/packages/container_registry/
              text: Learn More
              data_ga_name: container registry learn more
              data_ga_location: body
          - title: Helm Chart Registry
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Kubernetes cluster integrations can take advantage of Helm charts to standardize their distribution and install processes. Supporting a built-in helm chart registry allows for better, self-managed container orchestration.
            link:
              href: https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts
              text: Learn More
              data_ga_name: helm chart registry learn more
              data_ga_location: body
          - title: Dependency Proxy
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              The GitLab Dependency Proxy can serve as an intermediary between your local developers and automation and the world of packages that need to be fetched from remote repositories. By adding a security and validation layer to a caching proxy, you can ensure reliability, accuracy, and auditability for the packages you depend on.
            link:
              href: https://docs.gitlab.com/ee/user/packages/dependency_proxy/
              text: Learn More
              data_ga_name: dependency proxy learn more
              data_ga_location: body
          - title: Dependency Firewall
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              GitLab ensures that the dependencies stored in your package registries conform to your corporate compliance guidelines. This means you can prevent your organization from using dependencies that are insecure or out of policy.
            link:
              href: /direction/package/#dependency-firewall
              text: Learn More
              data_ga_name: dependency firewall learn more
              data_ga_location: body
          - title: Git LFS
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching.
            link:
              href: https://docs.gitlab.com/ee/topics/git/lfs/index.html
              text: Learn More
              data_ga_name: git lfs learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/enablement/){data-ga-name="enablement direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Release
            icon:
              url: /nuxt-images/icons/devops/release-colour.svg
              alt: Release
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/release/
              data_ga_name: release
              data_ga_location: body
          - title: Configure
            icon:
              url: /nuxt-images/icons/devops/configure-colour.svg
              alt: Configure
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Configure your applications and infrastructure.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/configure/
              data_ga_name: configure
              data_ga_location: body
          - title: Monitor
            icon:
              url: /nuxt-images/icons/devops/monitor-colour.svg
              alt: Monitor
            aos_animation: zoom-in
            aos_duration: 1500
            text: Help reduce the severity and frequency of incidents.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/monitor/
              data_ga_name: monitor
              data_ga_location: body
