---
  components:
    - name: copy
      data:
        block:
          - hide_horizontal_rule: true
            no_margin_bottom: true
            text: |
              GitLab offers a variety of support options for all customers and users on both paid and free tiers. You should be able to find help using the resources linked below, regardless of how you use GitLab. There are many ways to [contact Support](#contact-support){data-ga-name="contact support" data-ga-location="body"}, but the first step for most people should be to [search our documentation](https://docs.gitlab.com){data-ga-name="documentation" data-ga-location="body"}.

              If you can't find an answer to your question, or you are affected by an outage, then customers who are in a **paid** tier should start by consulting our [statement of support](/support/statement-of-support.html){data-ga-name="statement of support" data-ga-location="body"} while being mindful of what is outside of the scope of support. Please understand that any support that might be offered beyond the scope defined there is done at the discretion of the agent or engineer and is provided as a courtesy.

              If you're using one of GitLab's **free** options, please refer to the appropriate section for free users of either [self-managed GitLab](/support/statement-of-support.html#core-and-community-edition-users){data-ga-name="self-managed GitLab" data-ga-location="body"} or [on GitLab.com](/support/statement-of-support.html#free-plan-users){data-ga-name="free plan users" data-ga-location="body"}.

              Note that free GitLab Ultimate Self-managed and SaaS  granted through trials or as part of our [GitLab for Education](/solutions/education/){data-ga-name="GitLab for education" data-ga-location="body"}, [GitLab for Open Source](/solutions/open-source/){data-ga-name="GitLab for open source" data-ga-location="body"}, or [GitLab for Startups](/solutions/startups/){data-ga-name="GitLab for startups" data-ga-location="body"} programs do not come with technical support. Technical support for open source, education, and startup accounts can, however, be purchased at a significant discount by [contacting Sales](/sales/){data-ga-name="sales" data-ga-location="body"}. If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please refer to [Issues with billing, purchasing, subscriptions or licenses](#issues-with-billing-purchasing-subscriptions-or-licenses){data-ga-name="billing" data-ga-location="body"}.

              ## Contact Support {#contact-support}

              ### Issues with billing, purchasing, subscriptions or licenses {#issues-with-billing-purchasing-subscription-or-licenses}

              | Plan      | Support level (First Response Time)              | How to get in touch |
              |-----------|----------------------------|---------------------|
              | All plans and purchases | First reply within 8 hours, 24x5 | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="body"} and select "[Licensing and Renewals Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293){:data-ga-name="licensing and renewal problems" data-ga-location="body"}" |

              ### Self-managed (hosted on your own site/server){#self-managed-hosted-on-your-own-siteserver}

              | Plan | Support Level | How to get in touch |
              |------|----------------|---|
              | Free | None | Open a thread in the  [GitLab Community Forum](https://forum.gitlab.com){data-ga-name="forum" data-ga-location="body"} |
              | Premium and Ultimate | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="body"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="feature"} | \
              | | Tiered reply times based on [definitions of support impact](#definitions-of-support-impact){data-ga-name="definitions of support impact" data-ga-location="body"} |  For emergency requests, see the note in the [How to Trigger Emergency Support](#how-to-trigger-emergency-support){data-ga-name="emergency support" data-ga-location="body"} |
              | US Federal | [US Federal Support](#us-federal-support){data-ga-name="federal support" data-ga-location="body"} | Open a Support Ticket on the [GitLab Support Portal](https://gitlab-federal-support.zendesk.com/){data-ga-name="federal support portal" data-ga-location="body"} | \
              | | | For emergency requests, see the note in the [US Federal Support description](#us-federal-emergency-support){data-ga-name="federal emergency support" data-ga-location="body"} |

              ### GitLab SaaS (GitLab.com) {#gitlab-saas-gitlabcom}

              | Plan | Support Level | How to get in touch |
              |------|----------------|---|
              | Free |  None | Open a thread in the [GitLab Community Forum](https://forum.gitlab.com){data-ga-name="forum" data-ga-location="SaaS"} |
              | Premium and Ultimate | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="SaaS"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447){data-ga-name="support portal" data-ga-location="SaaS"} | \
              | | Tiered reply times based on [definitions of support impact](#definitions-of-support-impact){data-ga-name="definitions of support impact" data-ga-location="SaaS"} | |

              ### Legacy Plans {#legacy-plans}

              These plans are no longer offered for new purchases, but still maintain their historical support levels.

              | Plan | Support Level | How to get in touch |
              |------|----------------|---|
              | Bronze (GitLab.com) | [Standard Support](#standard-support-legacy){data-ga-name="standard support" data-ga-location="body"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="legacy"} | \
              | | Reply within 24 hours 24x5 | |
              | Starter (GitLab Self-managed) |  [Standard Support](#standard-support-legacy){data-ga-name="standard support" data-ga-location="feature"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="legacy"} | \
              | | Reply within 24 hours 24x5 | |

              ### Partnership Support {#partnership-support}

              If you're a registered [GitLab Partner](/handbook/resellers/){data-ga-name="GitLab partner" data-ga-location="body"} you'll want to take a look at the relevant [GitLab Support - Partnerships](/handbook/support/partnerships/){data-ga-name="partnership support" data-ga-location="body"} section for detailed instructions for how to get in touch with Support.

              | Type | Support Level | How to get in touch |
              |------|----------------|---|
              | [Open Partners](/handbook/support/partnerships/open.html){data-ga-name="open partners" data-ga-location="body"} | [Standard Support](#standard-support-legacy){data-ga-name="standard support" data-ga-location="partnership"} or [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="partnership"} depending on customer's license type | Open a Support Ticket using the [GitLab Support Portal - Open Partner Form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000818199){data-ga-name="support portal" data-ga-location="partnership"} |
              | [Select Partners](/handbook/support/partnerships/select.html){data-ga-name="select partners" data-ga-location="body"} | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="select partnership"} | Open a Support Ticket using the [GitLab Support Portal - Select Partner Form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000837100){data-ga-name="support portal" data-ga-location="select partnership"} |
              | [Alliance Partners](/handbook/support/partnerships/alliance.html){data-ga-name="alliance partners" data-ga-location="body"} | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="alliance partnership"} | Open a Support Ticket using the [GitLab Support Portal - Alliance Partner Form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001172559){data-ga-name="support portal" data-ga-location="alliance partnership"} |

              ### Proving your Support Entitlement {#proving-your-support-entitlement}
              Depending on how you purchased, upon the creation of your first ticket GitLab Support may **not** be able to automatically detect your support entitlement. If that's the case, you will be asked to provide evidence that you have an active license or subscription. [A variety of types of proof](managing-support-contacts.html#proving-your-support-entitlement){data-ga-name="types to proof - support" data-ga-location="body"} are accepted.

              If you want to make sure contacts in your organization aren't required to submit this proof, please see our dedicated page on [Managing Support Contacts](managing-support-contacts.html){data-ga-name="managing support contacts" data-ga-location="body"} for how to get set up with a named set of contacts.
