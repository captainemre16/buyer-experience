/* eslint-disable */

// Adapted from the Swiftype quick start documentation: https://swiftype.com/documentation/site-search/crawler-quick-start
(function (w, d, t, u, n, s, e) {
  w['SwiftypeObject'] = n;
  w[n] =
    w[n] ||
    function () {
      (w[n].q = w[n].q || []).push(arguments);
    };
  s = d.createElement(t);
  e = d.getElementsByTagName(t)[0];
  s.async = 1;
  s.src = u;
  e.parentNode.insertBefore(s, e);
})(window, document, 'script', '//s.swiftypecdn.com/install/v2/st.js', '_st');

_st('install', 'Z4n7msKyctXXfJs66EKx', '2.0.0');
